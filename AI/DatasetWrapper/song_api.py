import sqlite3
from sqlite3 import Error


def get_songs_by_query(conn, query, params = ()):
    cur = conn.cursor()
    cur.execute(query, params)
    songs = cur.fetchall()
    return songs

if __name__ == '__main__':
    conn = None
    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(r"Subset.db")
    except Error as e:
        print("SQL ERROR:")
        print(e)
    query = "SELECT energy FROM SONGS;"#this query sorts only by song name 
    #query = "SELECT * FROM SONGS WHERE song_name = 'I Didn''t Mean To' AND artist_name LIKE '%Casual%';"#this one sorts by song name and artist name (optimal)
    #query = "SELECT * FROM SONGS WHERE artist_name = 'Casual';"#this one sorts by artist name only
    songs = get_songs_by_query(conn, query)#songs is a list object that will caontain  all the requested songs
    print(songs)