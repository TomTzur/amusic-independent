from Song import SongData
import hdf5_getters as getters
import os
import sqlite3
from sqlite3 import Error

#creates the song table with all the variables
def create_table(conn):
    create_table_sql = """ CREATE TABLE IF NOT EXISTS SONGS(
                                song_id integer PRIMARY KEY,
                                song_name text NOT NULL,
                                song_name_normalized text NOT NULL,
                                artist_name text NOT NULL,
                                artist_name_normalized text NOT NULL,
                                artist_familiarity text NOT NULL,
                                danceability double NOT NULL,
                                duration double NOT NULL,
                                energy double NOT NULL,
                                key integer NOT NULL,
                                key_confidence double NOT_NULL,
                                loudness double NOT NULL,
                                mode integer NOT NULL,
                                mode_confidence float NOT NULL,
                                tempo float NOT NULL,
                                time_signature integer NOT NULL,
                                time_signature_confidence integer NOT NULL
    );"""
    create_song_index = """CREATE INDEX IF NOT EXISTS idx_song_name 
                            ON songs (song_name_normalized, artist_name_normalized);"""#creates an index that sorts the rows by song names first, and then artist names second

    create_song_index = """CREATE INDEX IF NOT EXISTS idx_artist_name 
                            ON songs (artist_name_normalized);"""#creates an index that sorts the rows by artist names
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
        c.execute(create_song_index)
        c.execute(create_song_index)
    except Error as e:
        print("SQL ERROR:")
        print(e)

#iterate over all the songs in the hdf5 filebase and migrate them into our database
def put_songs_in_sqlite_db(conn):

    trans_flag = True#flag that marks if our transaction was successful
    cur = conn.cursor()
    cur.execute("BEGIN;")

    try:
        motherDir = os.fsencode(r"C:\Users\tomtz\Projects\Dataset Sandbox\MillionSongSubset\data")
        for subDir1 in os.listdir(motherDir):#iterating over the first lvl of A-Z (MillionSongSubset\data\A)
            subDir1 = motherDir + b"\\" + subDir1
            for subDir2 in os.listdir(subDir1):##iterating over the second level of A-Z (MillionSongSubset\data\A\A)
                subDir2 = subDir1 + b"\\" + subDir2
                for subDir3 in os.listdir(subDir2):##iterating over the third level of A-Z (MillionSongSubset\data\A\A\A)
                    subDir3 = subDir2 + b"\\" + subDir3
                    for filePath in os.listdir(subDir3):
                        filePath = subDir3 + b"\\" + filePath
                        h5 = getters.open_h5_file_read(filePath)
                        for i in range(0, getters.get_num_songs(h5)):
                            songData = SongData(h5, i)
                            insert_song(conn, songData)
                        h5.close()
                        break
                    break
                break
            break
                        

    except Error as e:#if an error occured
        print("SQL ERROR:")
        print(e)
        trans_flag = False
        cur.execute("ROLLBACK;")
    if trans_flag:
        cur.execute("COMMIT;")

#this function inserts a specific song into the given table
def insert_song(conn, song):

    query = """INSERT INTO SONGS (song_name, song_name_normalized, artist_name, artist_name_normalized, artist_familiarity, danceability, duration, energy, key, key_confidence, loudness, mode, mode_confidence, tempo, time_signature, time_signature_confidence)
                    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"""#insert all these variables into the table. the "?" means that the variable will come in an additional tuple
    data = (song.song_name, song.song_name_normalized, song.artist_name, song.artist_name_normalized, song.artist_familiarity, str(song.danceability), str(song.duration), str(song.energy), str(song.key), str(song.key_confidence), str(song.loudness), str(song.mode), str(song.mode_confidence), str(song.tempo), str(song.time_signature), str(song.time_signature_confidence))#the tuple that contains the data for the row
    cur = conn.cursor()
    cur.execute(query, data)#the command and the data for that command

if __name__ == '__main__':
    conn = None
    try:#try to connect to the sqlite3 file
        conn = sqlite3.connect(r"OneSong.db")
    except Error as e:
        print("SQL ERROR:")
        print(e)
    
    create_table(conn)#create the table
    put_songs_in_sqlite_db(conn)#migrate songs from hdf5 to sqlite3
    cur = conn.cursor()#create a cursor (it executes commands for us)
    cur.execute("SELECT * FROM SONGS;")#just an example
    print(cur.fetchall())